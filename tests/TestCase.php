<?php

namespace Karls\Core\Tests;

use Karls\Core\CoreServiceProvider;

class TestCase extends \Tests\TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            CoreServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}
