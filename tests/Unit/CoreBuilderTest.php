<?php

namespace Tests\Unit;

use Exception;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Karls\Core\Builder\CoreBuilder;
use Karls\Core\Models\CoreModel;
use Karls\Core\Models\JoinType;
use Karls\Core\Tests\TestCase;

class CoreBuilderTest extends TestCase
{
    public static function columnInfosDataProvider(): array
    {
        return [
            'only column info' => ['id', [], 'licences.id'],
            'column with one relation' => ['id', ['licence'], 'licences.id'],
            'column with one relation plus initial' => ['id', ['licence', 'driver'], 'drivers.id'],
            'column with multiple relations' => ['id', ['driver', 'cars'], 'drivers|cars.id'],
            'column with multiple relations plus initial' => [
                'id', ['licence', 'driver', 'cars'], 'drivers|cars.id'
            ],
            'column with relation in column' => ['licence.id', [], 'licences.id'],
            'column with one relation in column' => ['driver.id', [], 'drivers.id'],
            'column with one relation in column plus initial' => [
                'licence.driver.id', [], 'drivers.id'
            ],
            'column with multiple relation in column' => ['driver.cars.id', [], 'drivers|cars.id'],
            'column with multiple relation in column plus initial' => [
                'licence.driver.cars.id', [], 'drivers|cars.id'
            ],
        ];
    }

    public function setup(): void
    {
        parent::setUp();
        Schema::create('cars', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('id')->primary();
            $t->uuid('driver_id')->nullable();
            $t->uuid('garage_id')->nullable();
        });
        Schema::create('drivers', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('id')->primary();
            $t->uuid('licence_id')->nullable();
        });
        Schema::create('licences', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('id')->primary();
            $t->uuid('car_id')->nullable();
        });
        Schema::create('garages', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('id')->primary();
        });
        Schema::create('owners', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('id')->primary();
        });
        Schema::create('car_owner', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('car_id');
            $t->uuid('owner_id');
            $t->primary(['car_id', 'owner_id']);
        });

        // morphTo relations
        Schema::create('posts', function (Blueprint $table) {
            $table->temporary();
            $table->uuid('id')->primary();
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->temporary();
            $table->uuid('id')->primary();
            $table->uuid('commentable_typeId');
            $table->string('commentable_type');
        });
    }

    public function test_joinByRel()
    {
        Car::create()->driver()->associate(Driver::create())->save();

        $compare = collect([
            Car::query()->joinByRel('driver'),
            Car::query()->join('drivers', 'drivers.id', 'cars.driver_id'),
        ]);

        $this->compareQueries($compare);

        $compare = collect([
            Driver::query()->joinByRel('cars'),
            Driver::query()->join('cars', 'drivers.id', 'cars.driver_id'),
        ]);

        $this->compareQueries($compare);

        $this->assertThrows(
            fn() => Driver::query()->joinByRel('NOT_A_RELATION')->get(),
            RelationNotFoundException::class,
        );
    }

    public function test_joinByRel_belongsToMany()
    {
        $car = Car::create();
        $car->driver()->associate(Driver::create())->save();
        $car->owners()->attach(Owner::create());
        $car->owners()->attach(Owner::create());

        $query = Car::query()->joinByRel('owners');
        self::assertEquals(2, count($query->getQuery()->joins));
    }

    public function test_morph_to_relation()
    {
        $post = Post::create();
        $commentOnPost = Comment::create([
            'commentable_typeId' => $post->id,
            'commentable_type' => Post::class,
        ]);

        $query = Comment::query()->joinByRel('post', morphTo: Post::class);
        $result = $query->get();

        self::assertCount(1, $result);
        self::assertDatabaseHas('posts', ['id' => $commentOnPost->commentable_typeId]);
    }

    public function test_is_joined_only_once_successfull()
    {
        Car::create()->driver()->associate(Driver::create())->save();

        /** @var CoreBuilder $query */
        $query = Driver::query()->joinByRel('cars');
        $query->joinByRel('cars');
        $query->get();
        self::assertEquals(1, count($query->getQuery()->joins));
    }

    public function test_use_left_joins()
    {
        Car::create()->driver()->associate(Driver::create())->save();

        $query = Driver::query()->joinByRel('cars', joinType: JoinType::LEFT);
        self::assertEquals('left', $query->getQuery()->joins[0]->type);

        $query = Driver::query()->joinByRel('cars');
        self::assertNotEquals('left', $query->getQuery()->joins[0]->type);
    }

    /*
     * Test, wo die Relations aus BelongsTo bestehen.
     */
    public function test_different_source_join()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();

        $query = Car::query()->joinByRel('driver', joinType: JoinType::LEFT);
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: 'driver');

        self::assertCount(2, $query->getQuery()->joins);
    }

    /*
     * Test, wo die Relations aus HasOne bzw. HasMany bestehen.
     */
    public function test_opposite_relations_multi_join()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();

        $query = Licence::query()->joinByRel('driver', joinType: JoinType::LEFT);
        $query->joinByRel('cars', joinType: JoinType::LEFT, source: 'driver');

        self::assertCount(2, $query->getQuery()->joins);
    }

    public function test_deep_sources()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $garage = Garage::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();
        $car->garage()->associate($garage)->save();

        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars', 'driver']);

        self::assertNotEmpty($query->get());
        self::assertCount(3, $query->getQuery()->joins);
    }

    public function test_multiple_joins_licence_different_paths()
    {
        $licence = Licence::create();
        $carLicence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $garage = Garage::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();
        $car->garage()->associate($garage)->save();
        $carLicence->car()->associate($car)->save();

        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars']);
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars', 'driver']);

        self::assertCount(4, $query->getQuery()->joins);
    }

    public function test_is_joined_simple()
    {
        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);

        // this one is already joined and can be ignored
        $query->joinByRel('cars', joinType: JoinType::LEFT);

        self::assertCount(1, $query->getQuery()->joins);
    }

    public function test_is_joined_with_one_source()
    {
        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');

        // this one is already joined and can be ignored
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');

        self::assertCount(2, $query->getQuery()->joins);
    }

    public function test_is_joined_with_multiple_source()
    {
        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars', 'driver']);

        // this one is already joined and can be ignored
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars', 'driver']);

        self::assertCount(3, $query->getQuery()->joins);
    }

    public function test_where_rel_successfull()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $driver->licence()->associate($licence)->save();

        $query = Licence::query()->joinByRel('driver', joinType: JoinType::LEFT);
        $query->whereRel('id', $driver->id, relations: ['driver']);

        self::assertNotEmpty($query->get());
    }

    public function test_where_rel_without_relation()
    {
        $licence = Licence::create();
        $query = Licence::query()->whereRel('id', $licence->id);

        self::assertNotEmpty($query->get());
    }

    public function test_multiple_where_rel()
    {
        $licence = Licence::create();
        $carLicence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $garage = Garage::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();
        $car->garage()->associate($garage)->save();
        $carLicence->car()->associate($car)->save();

        $query = Garage::query()->joinByRel('cars', joinType: JoinType::LEFT);
        $query->joinByRel('driver', joinType: JoinType::LEFT, source: 'cars');
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars']);
        $query->joinByRel('licence', joinType: JoinType::LEFT, source: ['cars', 'driver']);
        $query->whereRel('id', $carLicence->id, relations: ['cars', 'licence']);
        $query->whereRel('id', $licence->id, relations: ['cars', 'driver', 'licence']);

        self::assertNotEmpty($query->get());
    }

    public function test_or_where_rel()
    {
        $lic1 = Licence::create();
        $lic2 = Licence::create();

        self::assertEquals(2,
            Licence::query()
                ->whereRel('id', $lic1->id, ['licence'])
                ->orWhereRel('id', $lic2->id, ['licence'])
                ->count()
        );
    }

    public function test_where_in_rel()
    {
        $lic1 = Licence::create();
        $lic2 = Licence::create();

        $values = [
            $lic1->id,
            $lic2->id
        ];

        self::assertEquals(2,
            Licence::query()->whereInRel('id', $values)->count()
        );
    }

    public function test_simple_select()
    {
        $licence = Licence::create();

        $result = Licence::query()->selectRel('id')->first();

        self::assertEquals($licence->id, $result->id);
        self::assertCount(1, $result->toArray());
    }

    public function test_select_after_join()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();

        $query = Licence::query()->joinByRel('driver', joinType: JoinType::LEFT);
        $query->joinByRel('cars', joinType: JoinType::LEFT, source: 'driver');
        $query->selectRel('id')
            ->selectRel('id', ['driver'], 'driverId')
            ->selectRel('id', ['driver', 'cars'], 'carId');

        self::assertCount(3, $query->first()->toArray());
    }

    public function test_multiple_select_with_same_name()
    {
        $licence = Licence::create();
        $driver = Driver::create();
        $car = Car::create();
        $driver->licence()->associate($licence)->save();
        $car->driver()->associate($driver)->save();

        $query = Licence::query()->joinByRel('driver', joinType: JoinType::LEFT);
        $query->joinByRel('cars', joinType: JoinType::LEFT, source: 'driver');
        $query->selectRel('id')
            ->selectRel('id', ['driver'])
            ->selectRel('id', ['driver', 'cars']);

        self::assertCount(1, $query->first()->toArray());
    }

    /**
     * @dataProvider columnInfosDataProvider
     */
    public function test_get_column_name($columnName, $relation, $result)
    {
        $query = Licence::query();

        self::assertEquals($result, $query->getColumnName($columnName, $relation));
    }

    // getAlias Test
    public function test_getAlias()
    {
        $query = Licence::query();
        self::assertEquals('drivers', $query->getAlias(['driver']));
        self::assertEquals('drivers|cars', $query->getAlias(['driver', 'cars']));
        self::assertEquals('drivers|cars|garages', $query->getAlias(['driver', 'cars', 'garage']));
        self::assertEquals('drivers|cars', $query->getAlias('driver.cars'));
        self::assertEquals('drivers', $query->getAlias('driver'));

        // with initial licence
        self::assertEquals('drivers', $query->getAlias('licence.driver'));
        self::assertEquals('drivers', $query->getAlias(['licence', 'driver']));
        self::assertEquals('drivers|cars', $query->getAlias(['licence', 'driver', 'cars']));
        self::assertEquals('drivers|cars', $query->getAlias('licence.driver.cars'));
        self::assertEquals('drivers|cars|garages', $query->getAlias('licence.driver.cars.garage'));

        // only licence
        self::assertEquals('', $query->getAlias('licence'));
    }

    protected function compareQueries(Collection $c): void
    {
        $counts = $c->map(fn(CoreBuilder $q) => $q->count());
        static::assertEquals($counts[0], $counts[1]);

        $results = $c->map(fn(CoreBuilder $q) => $q->first());
        static::assertEquals($results[0], $results[1]);
    }
}

class Garage extends CoreModel
{
    public function cars(): HasMany
    {
        return $this->hasMany(Car::class);
    }
}

class Car extends CoreModel
{
    public function driver(): BelongsTo
    {
        return $this->belongsTo(Driver::class);
    }

    public function garage(): BelongsTo
    {
        return $this->belongsTo(Garage::class);
    }

    public function licence(): HasOne
    {
        return $this->hasOne(Licence::class);
    }

    public function owners(): BelongsToMany
    {
        return $this->belongsToMany(Owner::class);
    }
}

class Driver extends CoreModel
{
    public function cars(): HasMany
    {
        return $this->hasMany(Car::class);
    }

    public function licence(): BelongsTo
    {
        return $this->belongsTo(Licence::class);
    }
}

class Licence extends CoreModel
{
    public function driver(): HasOne
    {
        return $this->hasOne(Driver::class);
    }

    public function car(): BelongsTo
    {
        return $this->belongsTo(Car::class);
    }
}

class Owner extends CoreModel
{
    public function car(): BelongsToMany
    {
        return $this->belongsToMany(Car::class);
    }
}

class Comment extends CoreModel
{
    protected $guarded = [];
    public function postFiltered(): MorphTo
    {
        $morph = $this->morphTo('post', 'commentable_type', 'commentable_typeId');
        !$morph && throw new Exception('MorphTo relationship could not be resolved.');

        return $morph->getModel()->getTable() === 'post'
            ? $morph
            : $morph->whereRaw("0=1");
    }

    public function post(): MorphTo
    {
        $morph = $this->morphTo('post', 'commentable_type', 'commentable_typeId');
        return $morph?->first()?->commentable_type === 'post'
        || $morph->getModel()->getTable() === 'posts'
            ? $morph
            : $morph->whereRaw("0=1");
    }
}

class Post extends CoreModel
{
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'commentable_id');
    }
}
