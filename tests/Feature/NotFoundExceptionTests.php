<?php

namespace Karls\Core\Tests\Feature;

use Faker\Provider\Uuid;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Karls\Core\Models\CoreModel;
use Karls\Core\Tests\TestCase;
use Karls\ErrorHandling\Exceptions\ErrorCode as CoreErrorCode;
use Symfony\Component\HttpFoundation\Response;

class NotFoundExceptionTests extends TestCase
{
    const INVALID_ROUTE = '/invalidRoute';
    const TESTING_TABLE = '__TESTING_TABLE_';

    public function test_route_not_found()
    {
        $response = $this->get(self::INVALID_ROUTE);
        $response
            ->assertJson([
                'code' => CoreErrorCode::HTTP_NOT_FOUND,
                'message' => __('coreError::message.urlNotFound', [
                    'url' => join([
                        Config::get('app.url'),
                        self::INVALID_ROUTE
                    ])
                ])
            ])
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_model_not_found()
    {
        try {
            Schema::create(self::TESTING_TABLE, fn(Blueprint $t) => $t->uuid('id'));
            $modelRoute = '/model';
            Route::post(join('/', [$modelRoute, '{m}', 'get']), function (TestModel $m) {
                return $m->id;
            });

            $this->post(join('/', [$modelRoute, Uuid::uuid(), 'get']))
                ->assertStatus(Response::HTTP_NOT_FOUND);
        } finally {
            Schema::dropIfExists(self::TESTING_TABLE); // needs to be executed manually, if process is killed
        }
    }
}

class TestModel extends CoreModel
{
    protected $table = NotFoundExceptionTests::TESTING_TABLE;
}
