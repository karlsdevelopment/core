<?php

namespace Karls\Core;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Karls\Core\Console\ShowTablesCommand;

class CoreServiceProvider extends ServiceProvider
{
    public array $bindings = [];

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/core.php', 'core');
        parent::register();
    }

    public function boot()
    {
        // publish config files
        $this->publishes([
            __DIR__ . '/../config/core.php' => config_path('karls-core.php'),
        ]);

        // TODO move adding from Kernel to serviceProvider
//        app('router')->prependMiddlewareToGroup('api', ApiMiddleware::class);

        // add helper
        File::exists(__DIR__ . '/helper.php')
        && require_once __DIR__ . '/helper.php';

        if ($this->app->runningInConsole()) {
            $this->commands([
                ShowTablesCommand::class,
            ]);
        }
    }
}
