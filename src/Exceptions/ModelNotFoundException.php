<?php


namespace Karls\Core\Exceptions;

use Exception;
use Throwable;

class ModelNotFoundException extends Exception
{
    public function __construct(
        string $model,
        int $code = 0,
        ?Throwable $previous = null
    )
    {
        parent::__construct(
            "Resolved related model class '{$model}' does not exist.",
            $code,
            $previous
        );
    }
}
