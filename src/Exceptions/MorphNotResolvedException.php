<?php


namespace Karls\Core\Exceptions;

use Exception;
use Throwable;

class MorphNotResolvedException extends Exception
{
    public function __construct(
        int $code = 0,
        ?Throwable $previous = null
    )
    {
        parent::__construct("Morph type is not set or resolved.", $code, $previous);
    }
}
