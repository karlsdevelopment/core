<?php

use Illuminate\Http\JsonResponse;

function successResponse(array $response): JsonResponse
{
    if (!isset($response['data'])) {
        $data = &$response;
        unset($response);
        $response['data'] = $data;
    }

    return response()->json($response);
}
