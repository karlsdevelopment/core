<?php

namespace Karls\Core\Builder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Relationship;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Str;
use Karls\Core\Exceptions\ModelNotFoundException;
use Karls\Core\Exceptions\MorphNotResolvedException;
use Karls\Core\Models\JoinType;

class CoreBuilder extends Builder
{
    protected const DELIMITER = '|';
    protected const MORPH_SUFFIX = 'Filtered';

    protected string $modelClass;

    public function __construct(QueryBuilder $query = null)
    {
        if (isset($query)) {
            parent::__construct($query);
            return;
        }

        $instance = new $this->getModelClass();
        parent::__construct($instance->newBaseQueryBuilder());
        $this->setModel($instance);
    }

    public static function init(): static
    {
        return new static();
    }

    public function getModelClass(): string
    {
        return $this->modelClass ?? $this->getModel()::class;
    }

    public static function builderForModel(string $modelName): string
    {
        $builder = $modelName . 'Builder';

        !class_exists($builder)
        && $builder = Str::replace('\Models\\', '\Builders\\', $builder);

        return class_exists($builder) ? $builder : static::class;
    }

    public function isJoined(string $table): bool
    {
        return collect($this->getQuery()->joins ?? [])
            ->pluck('type', 'table')
            ->keys()
            ->map(fn($key) => preg_replace('/\b\w+\sas\s/', '', $key)) // Clean up the keys
            ->contains(fn($key) => $key === $table || in_array($table, explode('|', $key)));
    }

    public function findJoin(string $table): ?string
    {
        $contains = collect($this->getQuery()->joins);
        $pluck = $contains->pluck('type', 'table');
        $join = $pluck->filter(fn($value, $key) => str_starts_with($key, $table));

        if ($join->isEmpty()) {
            return null;
        }

        $parts = explode(' as ', $join->keys()->first());

        if ($parts === false) {
            return null;
        }

        return end($parts);
    }

    public function joinType(string $table): ?string
    {
        $joins = collect($this->getQuery()->joins)
            ->pluck('type', 'table');
        return $joins[$table] ?? null;
    }

    public function joinByRel(
        Relationship|string $rel,
        JoinType $joinType = JoinType::INNER,
        string|array $source = null,
        ?string $morphTo = null
    ): static
    {
        $modelClass = $this->getModelClass();
        $alias = null;

        if (!empty($source)) { // set table to join to
            $source = !is_array($source) ? [$source] : $source;
            $alias = $this->getAlias(array_merge($source, [$rel]), $morphTo);

            foreach ($source as $src) {
                $src === end($source)
                && $morphTo === $modelClass
                && $src .= self::MORPH_SUFFIX;

                $modelClass = method_exists($modelClass, $src)
                    ? get_class((new $modelClass)->$src()->getRelated())
                    : throw RelationNotFoundException::make((new $modelClass), $src);
            }
        }

        if (is_string($rel)) { // convert relation string to model
            $rel .= $morphTo ? self::MORPH_SUFFIX : '';
            $rel = method_exists($modelClass, $rel)
                ? (new $modelClass)->$rel()
                : throw RelationNotFoundException::make((new $modelClass), $rel);
        }

        return $this->_joinByRel($rel, $joinType, $morphTo, $alias);
    }

    private function joinMorphToByRel(
        Relation $rel,
        JoinType $joinType = JoinType::INNER,
        ?string $morphTo = null,
    ): static
    {
        $morphValue = $morphTo ?? (new $this->getModelClass)->getAttribute(
            $rel->getMorphType()
        );
        !$morphValue && throw new MorphNotResolvedException();
        $relatedModelClass = Relation::getMorphedModel($morphValue) ?? $morphValue;
        !class_exists($relatedModelClass)
        && throw new ModelNotFoundException($relatedModelClass);

        $relatedTable = (new $relatedModelClass)->getTable();
        $foreignKey = $rel->getMorphType() . 'Id';
        $ownerKey = (new $relatedModelClass)->getKeyName();

        return $this->{$joinType->value}(
            $relatedTable,
            "{$relatedTable}.{$ownerKey}",
            "{$rel->getParent()->getTable()}.{$foreignKey}"
        );
    }

    private function joinBelongsToManyByRel(
        Relation $rel,
        JoinType $joinType = JoinType::INNER,
        ?string $alias = null,
    ): static
    {
        $pivotTable = $rel->getTable();
        $relatedTable = $rel->getRelated()->getTable();

        if (!$this->isJoined($pivotTable)) {
            $this->{$joinType->value}(
                $pivotTable,
                $rel->getQualifiedParentKeyName(),
                $pivotTable . '.' . $rel->getForeignPivotKeyName()
            );
        }

        if (!$this->isJoined($alias ?? $relatedTable)) {
            $this->{$joinType->value}(
                $relatedTable . ($alias !== null ? ' as ' . $alias : ''),
                $pivotTable . '.' . $rel->getRelatedPivotKeyName(),
                ($alias !== null ? $alias : $relatedTable) . '.' . $rel->getRelatedKeyName()
            );
        }

        return $this;
    }

    private function _joinByRel(
        Relation $rel,
        JoinType $joinType = JoinType::INNER,
        ?string $morphTo = null,
        ?string $alias = null,
    ): static
    {

        if ($rel instanceof MorphTo) {
            return $this->joinMorphToByRel($rel, $joinType, $morphTo);
        }

        if ($rel instanceof BelongsToMany) {
            return $this->joinBelongsToManyByRel($rel, $joinType, $alias);
        }

        if ($this->isJoined($alias ?? $rel->getRelated()->getTable()))
            return $this;

        $joins = $this->findJoin($rel instanceof BelongsTo
            ? $rel->getChild()->getTable()
            : $rel->getParent()->getTable()
        );

        $foreignKey = $joins === null
            ? $rel->getQualifiedForeignKeyName()
            : ($rel instanceof BelongsTo
                ? $joins . '.' . $rel->getForeignKeyName()
                : $joins . '.' . $rel->getLocalKeyName());

        $this->{$joinType->value}(
            $rel->getRelated()->getTable() . ($alias != null ? ' as ' . $alias : ''),
            static::getRelationOwnerKey($rel, $alias),
            $foreignKey
        );

        return $this;
    }

    /** Get the primarykey's name, of the table, that roots the join (is joined to) */
    private static function getRelationOwnerKey(Relation $rel, ?string $alias)
    {
        return ($alias === null)
            ? ($rel instanceof BelongsTo
                ? $rel->getQualifiedOwnerKeyName()
                : $rel->getQualifiedParentKeyName())
            : ($alias . '.' . ($rel instanceof BelongsTo
                    ? $rel->getOwnerKeyName()
                    : $rel->getForeignKeyName()));
    }

    public function whereRel(
        $column,
        $operator = null,
        $value = null,
        $boolean = 'and',
        array $relations = []
    ): static
    {
        return $this->where(
            $this->getColumnName($column, $relations),
            $operator,
            $value,
            $boolean
        );
    }

    public function orWhereRel(
        $column,
        $operator = null,
        $value = null,
        array $relations = []
    ): static
    {
        return $this->orWhere(
            $this->getColumnName($column, $relations),
            $operator,
            $value
        );
    }

    public function whereInRel(
        $column,
        $value = null,
        $boolean = 'and',
        $not = false,
        array $relations = []
    ): static
    {
        return $this->whereIn(
            $this->getColumnName($column, $relations),
            $value,
            $boolean,
            $not
        );
    }

    public function selectRel(
        $column,
        array $relations = [],
        string $alias = '',
        ?string $morphTo = null
    ): static
    {
        $columnName = $this->getColumnName($column, $relations, $morphTo);
        $columnWithAlias = $alias ? "$columnName as $alias" : $columnName;

        return $this->addSelect($columnWithAlias);
    }

    public function getColumnName(
        Relation|string $column,
        array $relations = [],
        ?string $morphTo = null
    ): string
    {
        if ($this->_hasRelations($column)) {
            $list = explode('.', $column);
            $column = last($list);
            $relations = array_slice($list, 0, -1);
        }

        $relation = $this->getAlias($relations, $morphTo);
        if (empty($relation)) {
            $relation = new($this->getModelClass());
            $relation = $relation->getTable();
        }

        return $relation . '.' . $column;
    }

    public function getAlias(
        string|array $relations,
        ?string $morphTo = null
    ): string
    {
        if (empty($relations)) return '';
        if ($morphTo) return (new $morphTo)->getTable();

        $modelClass = $this->getModelClass();

        if (is_string($relations)) {
            $relations = explode('.', $relations);
        }

        $tables = [];
        $isFirst = true;
        foreach ($relations as $relation) {
            // if initial $modelClass === $relation continue
            if ($isFirst && Str::lower(class_basename(new $modelClass)) === $relation) {
                continue;
            }

            $modelClass = method_exists($modelClass, $relation)
                ? (new $modelClass)->$relation()->getRelated()
                : throw RelationNotFoundException::make((new $modelClass), $relation);
            $tables[] = $modelClass->getTable();
            $isFirst = false;
        }

        return implode(self::DELIMITER, $tables);
    }

    private function _hasRelations($column): bool
    {
        return count(explode('.', $column)) > 1;
    }
}

