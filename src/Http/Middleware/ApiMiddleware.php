<?php

namespace Karls\Core\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Throwable;

class ApiMiddleware
{
    public function handle(
        Request $request,
        Closure $next
    ): JsonResponse|BinaryFileResponse|StreamedResponse
    {
        try {
            $response = $next($request);
        } catch (Throwable $e) {
            while (DB::transactionLevel() > 0) {
                DB::rollBack();
            }
            throw $e;
        }

        if ($response instanceof StreamedResponse
            || $response instanceof BinaryFileResponse) {
            return $response;
        }

        $data = json_decode($response->getContent(), true);
        if (isset($data['code'], $data['message'])) {
            return $response;
        }

        $content = $response instanceof JsonResponse
            ? json_decode($response->getContent(), true)
            : [$response->getContent()];

        return successResponse($content);
    }
}
