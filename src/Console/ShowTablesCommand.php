<?php

namespace Karls\Core\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ShowTablesCommand extends Command
{
    protected $signature = 'db:tables';

    protected $description = 'Shows information about all tables of the project';

    public function handle()
    {
        $results = DB::table("information_schema.TABLES")
            ->select('table_name')
            ->selectRaw("ROUND(((data_length + index_length) / 1024 / 1024), 2)")
            ->where('table_schema', DB::connection()->getDatabaseName())
            ->orderByRaw('(data_length + index_length) desc')->get();

        $transformedResults = collect($results)
            ->transform(fn($obj) => (array)$obj)
            ->toArray();

        $this->table(['table', 'size (MB)'], $transformedResults);
    }
}
