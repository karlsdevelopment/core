<?php

namespace Karls\Core\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Karls\Core\Builder\CoreBuilder;
use Ramsey\Uuid\Uuid;

/**
 * @property Carbon $updatedAt
 */
abstract class CoreModel extends Model
{
    public const RULES = [];
    public const PAGE_SIZE = 25;

    public const CREATED_AT = 'createdAt';
    public const UPDATED_AT = 'updatedAt';
    public const DELETED_AT = 'deletedAt';

    public $timestamps = false;
    public $keyType = 'string';
    public $incrementing = false;
    public ?string $collection = null;
    public static $snakeAttributes = false;
    public static string $builder;

    public function newInstance($attributes = [], $exists = false): static
    {
        $model = parent::newInstance($attributes, $exists);
        $key = $model->getKeyName();
        $model->$key = $attributes[$key] ?? Uuid::uuid4();
        return $model;
    }

    public function update(array $attributes = [], array $options = []): bool
    {
        if (!$this->exists) {
            return false;
        }

        in_array('updatedAt', $this->fillable) && $this->updatedAt = now();
        return $this->fill($attributes)->save($options);
    }

    public function loadMissingPage($relation, $page = 0, $pageSize = null): static
    {
        return $this->loadMissing([
            $relation => fn($q) => $q->paginate(
                $pageSize ?? $this->$relation()->getRelated()::PAGE_SIZE,
                page: $page
            )
        ]);
    }

    public function newEloquentBuilder($query)
    {
        $builder = $this->builder ?? CoreBuilder::builderForModel(static::class);
        return new $builder($query);
    }

    public function newCollection(array $models = [])
    {
        $collection = $this->collection ?? (static::class . 'Collection');
        !class_exists($collection) && $collection = Collection::class;

        return new $collection($models);
    }
}
