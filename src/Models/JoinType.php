<?php
declare (strict_types=1);

namespace Karls\Core\Models;

enum JoinType: string
{
    case LEFT = 'leftJoin';
    case INNER = 'join';
}
